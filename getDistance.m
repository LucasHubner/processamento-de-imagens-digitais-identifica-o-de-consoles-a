function value = getDistance(im1, im2)
  imAux = imresize(im2, size(im1));
  value = corr2(imAux, im1);
 
 endfunction