function [imbb, newImage] = correctAngle(image, angle)
  image = imrotate(image, -angle);
  
  % Gambita para remover areas indesejadas
  regionCut = regionprops(image,'BoundingBox', 'Area');
  biggestArea = 0;
  biggestIndice = 1;
  for regionIndice = 1:length(regionCut)
    if regionCut(regionIndice).Area > biggestArea 
      biggestArea = regionCut.Area;
      biggestIndice = regionIndice;
    end
  end
  newImage = imcrop(image, regionCut(biggestIndice).BoundingBox);
  imbb = regionCut(biggestIndice).BoundingBox;
endfunction