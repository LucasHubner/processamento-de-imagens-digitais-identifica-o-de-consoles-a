function f = getModel (name)

modelPath = strcat("/home/lg/Dropbox/Matérias/PDI/Trabalho 2/Imagens/Modelos/", name);
modelPath = strcat(modelPath, ".jpg");
i = imread(modelPath);
original = i;
i = rgb2gray(i);
i = edge(i, "Canny");

disk = strel('disk', 5,0);
i = imdilate(i, disk);
i = bwfill(i,'holes');

i = bwareaopen(i,100000);
region = regionprops(i,'BoundingBox', 'Orientation', 'Area');

 % Faz o recorte das fitas separadas
  cut = imcrop(i,region.BoundingBox);
  % Rotaciona 
  cut = imrotate(cut, -region.Orientation);
  
    % Gambita para remover areas indesejadas
  regionCut = regionprops(cut,'BoundingBox', 'Area');
  biggestArea = 0;
  biggestIndice = 1;
  for regionIndice = 1:length(regionCut)
    if regionCut(regionIndice).Area > biggestArea 
      biggestArea = regionCut.Area;
      biggestIndice = regionIndice;
    end
  end
  cut = imcrop(cut, regionCut(biggestIndice).BoundingBox);
    
  %Transforma imagem de binario para inteiro
  % Desta maneira podemos utilizar o imresize
  cut = uint8(cut);
  cut = cut * 255;
  f = cut;
  
  endfunction