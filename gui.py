import gi
gi.require_version('Gtk','3.0')
from gi.repository import Gtk
from matplotlib.figure import Figure
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
import numpy
import cv2
from oct2py import octave

class MainWindow:
	def __init__(self):
		#Cria variaveis
		self.img = None
		self.mainCanvas = None
		self.imagePath = None

		# Inicia o Gtk e seta layout
		self.builder = Gtk.Builder()
		self.builder.add_from_file('mainwindow.glade')
		self.builder.connect_signals(self)

		# Captura objetos necessários e seta tamanho
		self.box = self.builder.get_object("innerBox")
		self.window = self.builder.get_object("mainWindow")
		self.window.set_default_size(800,600)
		self.window.maximize()
		self.window.show_all()
#-----------------------------------------------------------------------------#
	def onMenuCategorize(self, widget):
		octave.addpath("/home/lg/Dropbox/Matérias/PDI/Trabalho 2/")
		if(self.imagePath):
			octave.categorize(self.imagePath)

################################################################################
#Screnn Handlers
################################################################################
	#Menu Arquivo
	def onMenuOpen(self, widget):
		fileName = None

		# Criando Dialog para selecionar arquivo
		dialog = Gtk.FileChooserDialog("Please choose a file", self.window,
		Gtk.FileChooserAction.OPEN,
			(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
			Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

		# Adiciona filtro ao dialog para mostrar apenas imagens
		dialogFilters = Gtk.FileFilter()
		dialogFilters.set_name("Image files")
		dialogFilters.add_mime_type("image/*")

		# Adiciona filtro ao dialog
		dialog.add_filter(dialogFilters)

		response = dialog.run()

		#  Se o arquivo for selecionado, então seta fileName
		if response == Gtk.ResponseType.OK:
			fileName = dialog.get_filename()
			self.imagePath = fileName
		dialog.destroy()

		if (fileName):
				self.initialize(fileName)
#-----------------------------------------------------------------------------#
	def initialize(self, filePath):
		self.initialized = True
		img = cv2.imread(filePath, cv2.IMREAD_UNCHANGED)
		figure = self.getFigure(img,"Original")

		if(self.mainCanvas):
			self.box.remove(self.mainCanvas)

		self.mainCanvas = FigureCanvas(figure)
		self.box.pack_start(self.mainCanvas, True,True,0)
		self.window.show_all()
#-----------------------------------------------------------------------------#
	def getFigure(self, img, title):
		figure = Figure()
		ax = figure.add_subplot(111)
		try:
			# Mostra figure no canvas e seta origin como Lower (Pode inverter a imagem em alguns sistemas)
			ax.imshow(cv2.cvtColor(img,cv2.COLOR_BGR2RGB), origin='lower')
		except Exception as e:
			print(e)
			ax.imshow(img,'gray', origin='lower')
		ax.autoscale(False)
		ax.set_title(title)
		ax.set_xticks([])
		ax.set_yticks([])
		return figure
################################################################################
#Menu Ajuda
################################################################################
	def onMenuSobre(self, widget):
		aboutdialog = Gtk.AboutDialog()

		# lists of authors and documenters (will be used later)
		authors = ["Lucas Guilherme Hübner"]
		documenters = ["Lucas Guilherme Hübner"]

		# we fill in the aboutdialog
		aboutdialog.set_program_name("About")
		aboutdialog.set_copyright("Copyright \xa9 2012 Lucas Guilherme Hübner")
		aboutdialog.set_authors(authors)
		aboutdialog.set_documenters(documenters)
		aboutdialog.set_website("https://bitbucket.org/LucasHubner/processamento-de-imagens-digitais-identifica-o-de-consoles-a")
		aboutdialog.set_website_label("Fork on BitBucket")

		# we do not want to show the title, which by default would be "About AboutDialog Example"
		# we have to reset the title of the messagedialog window after setting
		# the program name
		aboutdialog.set_title("About")

		# show the aboutdialog
		aboutdialog.show()

#-----------------------------------------------------------------------------#

	def onDeleteWindow(self, *args):
		Gtk.main_quit(*args)
#################################################################
# End of the screen handlers
################################################################
	def notImplemented(self):
		messagedialog = Gtk.MessageDialog(parent=self.window,
		flags=Gtk.DialogFlags.MODAL,
		type=Gtk.MessageType.WARNING,
		buttons=Gtk.ButtonsType.OK,
		message_format="Função não implementada")
		# show the messagedialog
		response = messagedialog.run()

		messagedialog.destroy()

	################################################################################

if __name__ == '__main__':
	mv = MainWindow()
	Gtk.main()
