function categorize(filePath)
pkg load all;
%filePath = "/home/lg/Imagens/im2.jpg";
modelMega = getModel("megadrive");
modelN64 = getModel("n64");
modelFamicom = getModel("famicom");

i = imread(filePath);
original = i;
i = rgb2gray(i);
i = edge(i, "Canny");

disk = strel('disk', 5,0);
i = imdilate(i, disk);
i = bwfill(i,'holes');

i = bwareaopen(i,100000);
region = regionprops(i,'BoundingBox', 'Orientation', 'Area',"Centroid");

%imshow(original);
%hold

% para cada imagem encontrada, realiza processo.
for x = 1:length(region)
  % Faz o recorte das fitas separadas
  cut = imcrop(i,region(x).BoundingBox);
  cutOriginal = imcrop(original,region(x).BoundingBox);
  % Rotaciona e remove areas indesejadas
  [imbb, cut ]= correctAngle(cut,region(x).Orientation);
  cutOriginal = imrotate(cutOriginal, -region(x).Orientation);
  cutOriginal = imcrop(cutOriginal, imbb);
  
  %Transforma imagem de binario para inteiro
  % Desta maneira podemos utilizar o imresize
  cut = uint8(cut);
  cut = cut * 255;
  
  dist = getDistance(cut, modelMega);
  console = "Megadrive";
  dN64 = getDistance(cut, modelN64);
  if dist < dN64
    dist = dN64;
    console = "Nintendo 64";
  end
  dFamicom = getDistance(cut, modelFamicom);
  if dist < dFamicom
    dist = dFamicom;
    console = "Famicom";
  end
  
  %text(region(x).Centroid(1) - 650, region(x).Centroid(2),
  %     console,
  %     "fontsize", 40
  %     );
  
  figure(x), imshow(cutOriginal), title(console, "fontsize",40);
end

endfunction