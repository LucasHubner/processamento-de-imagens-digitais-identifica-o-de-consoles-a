filePath = "/home/lg/Imagens/im2.jpg";

i = imread(filePath);

i = rgb2gray(i);
%figure(1), subplot(2,3 ,1), imshow(i), title("rgb2gray","fontsize",35);

i = edge(i, "Canny");
%figure(1), subplot(2,3 ,2), imshow(i), title("edge~~Canny","fontsize",35);

disk = strel('disk', 5,0);
i = imdilate(i, disk);
%figure(1), subplot(2,3 ,3), imshow(i), title("imdilate","fontsize",35);

i = bwfill(i,'holes');
%figure(1), subplot(2,3 ,4), imshow(i), title("bwfill","fontsize",35);

i = bwareaopen(i,100000);
%figure(1), subplot(2,3 ,5), imshow(i), title("bwareaopen","fontsize",35);

region = regionprops(i,'BoundingBox', 'Orientation', 'Area',"Centroid");

for x = 1:length(region)
  % Faz o recorte das fitas separadas
  cut = imcrop(i,region(x).BoundingBox);
  [imbb, cut ]= correctAngle(cut,region(x).Orientation);

  figure(1), subplot(1,3 ,x), imshow(cut); %title("rgb2gray","fontsize",35);
end